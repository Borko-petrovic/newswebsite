import React, { Component } from "react";
import "./App.css";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import AppleArticles from "./components/AppleArticles";
import TeslaArticles from "./components/TeslaArticles";
import WallStreetArticles from "./components/WallStreetArticles";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <div className="container-fluid w-90 ">
          <div className="row">
            <div className="col ">
              <TeslaArticles />
            </div>
            <div className="col">
              <AppleArticles />
            </div>
            <div className="col">
              <WallStreetArticles />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
