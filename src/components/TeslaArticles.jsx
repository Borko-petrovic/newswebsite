import React, { useState, useEffect } from "react";

import useInfiniteScroll from "@closeio/use-infinite-scroll";

import styles from "./News.Module.css";

const URL =
  "https://newsapi.org/v2/everything?q=tesla&from=2021-05-01&sortBy=publishedAt&apiKey=23c88a1a6848408ab664af8ff4d50e9d";

export default function TeslaArticles() {
  const [items, setItems] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  const [page, loaderRef, scrollerRef] = useInfiniteScroll({ hasMore });

  useEffect(() => {
    (async () => {
      const realPage = page + 1;
      const resp = await fetch(`${URL}&page=${realPage}`);
      const data = await resp.json();
      setHasMore(realPage * 10 <= data.totalResults);
      setItems((prev) => [...prev, ...data.articles]);
      console.log(data.articles);
    })();
  }, [page]);

  return (
    <>
      <div className="bg-primary p-2">
        <h4>Tesla News</h4>
      </div>
      <div ref={scrollerRef} className={styles.scroller}>
        {items.map((item) => (
          <div className="card">
            <img
              className="card-img-top"
              src={item.urlToImage}
              alt="beaufel"
            ></img>
            <div className="card-body">
              <h5 className="card-title">{item.title}</h5>
              <p className="card-text">{item.description}</p>
              <a href={item.url} className="btn btn-primary">
              Read More
              </a>
            </div>
          </div>
        ))}
        {hasMore && <div ref={loaderRef}>Loading…</div>}
      </div>
    </>
  );
}
