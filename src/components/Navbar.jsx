import React from "react";

const Navbar = () => {
  return (
    <div className="Navbar">
      <nav
        class="navbar navbar-dark bg-info
        mb-5"
      >
        <div className="col-2">
          <a className="navbar-brand" href="https://newsapi.org/">
            <img
              src="https://image.flaticon.com/icons/png/512/21/21601.png"
              width="100"
              height="50"
              alt=""
            ></img>
          </a>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
